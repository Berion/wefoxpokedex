//
//  PokemonServiceTests.swift
//  WefoxPokedexTests
//
//  Created by Alberto García on 26/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import XCTest
import RxSwift
import OHHTTPStubs

@testable import WefoxPokedex

class PokemonServiceTests: XCTestCase {
    
    var sut: PokemonService!
    var disposeBag = DisposeBag()
    var pokemonMock = Pokemon(id: 222, name: "testPok", weight: 321, height: 321, image: "pokeimage", order: 321, caughtDate: Date(), baseExperience: 321, types: ["test", "typetest"])

    override func setUp() {
        sut = PokemonService()
    }

    override func tearDown() {
        sut = nil
    }

    func testSearchedPokemonHasMandatoryFields() {
        
        let expectation = XCTestExpectation(description: "Pokemon loaded")
        
        
        stub(condition: isHost("pokeapi.co")) { _ in
            let stubPath = OHPathForFile("pokemonStub.json", type(of: self))
            return fixture(filePath: stubPath!, headers: ["Content-Type": "application/json"])
        }
        
        var pokemonLoaded: Pokemon? = nil
        
        sut.searchPokemonById(pokemonId: 123123123).subscribe(onNext: { (pokemon) in
            
            print("pokemon")
            
            pokemonLoaded = pokemon
            
        }, onError: { (error) in
            
            XCTFail(error.localizedDescription)
            
        }, onCompleted: {
            
            if let pokemon = pokemonLoaded {
                
                XCTAssertEqual(pokemon.id, 123123123)
                XCTAssertEqual(pokemon.name, "testpokemon")
                XCTAssertEqual(pokemon.weight, 123.0)
                XCTAssertEqual(pokemon.height, 123.0)
                XCTAssertEqual(pokemon.image, "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/123123123.png")
                XCTAssertEqual(pokemon.order, 123123123)
                XCTAssertEqual(pokemon.baseExperience, 123123123)
                XCTAssertEqual(pokemon.types, ["test", "testing"])
                
            } else {
                
                XCTFail("Pokemon lot parsed")
                
            }
            
            
        }, onDisposed: {
            
            expectation.fulfill()
            
        }).disposed(by: disposeBag)
        
        wait(for: [expectation], timeout: 5)
        
    }
    
    
    func testPokemonIsSavedAndLoaded() {
        
        sut.savePokemon(pokemon: pokemonMock)
        
        let pokemonLoaded = sut.loadPokemonBy(id: pokemonMock.id!)
        
        XCTAssertEqual(pokemonMock.id, pokemonLoaded!.id)
        XCTAssertEqual(pokemonMock.baseExperience, pokemonLoaded!.baseExperience)
        XCTAssertEqual(Int(pokemonMock.caughtDate?.timeIntervalSinceReferenceDate ?? 0), Int(pokemonLoaded!.caughtDate?.timeIntervalSinceReferenceDate ?? 0))
        XCTAssertEqual(pokemonMock.height, pokemonLoaded!.height)
        XCTAssertEqual(pokemonMock.image, pokemonLoaded!.image)
        XCTAssertEqual(pokemonMock.name, pokemonLoaded!.name)
        XCTAssertEqual(pokemonMock.order, pokemonLoaded!.order)
        
        
    }

}
