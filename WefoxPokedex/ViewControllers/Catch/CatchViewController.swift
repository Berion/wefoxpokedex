//
//  CatchViewController.swift
//  WefoxPokedex
//
//  Created by Alberto García on 23/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AlamofireImage

class CatchViewController: UIViewController {
    
    private let viewModel = CatchViewModel(pokemonService: PokemonService())
    private let disposeBag = DisposeBag()
    private let pokemonId = Int.random(in: 1 ... 1000)
    private var pokemon: Pokemon? = nil

    @IBOutlet weak var pokemonImage: UIImageView!
    @IBOutlet weak var pokemonName: UILabel!
    @IBOutlet weak var pokemonWeight: UILabel!
    @IBOutlet weak var pokemonHeight: UILabel!
    @IBOutlet weak var catchButton: UIButton!
    
    @IBAction func leaveAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func catchPokemon(_ sender: Any) {
        
        if let pokemon = pokemon {
            
            viewModel.savePokemon(pokemon: pokemon)
            
            let alert = UIAlertController(title: "Catched!", message: "The pokemon was added to your bag", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "See the bag", style: .default, handler: {[weak self] (action) in
                self?.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bindOutputs()
        loadPokemonFromBag()
    }
    
    private func loadPokemonFromBag() {
        
        viewModel.loadPokemon(by: pokemonId).subscribe(onNext: { [weak self] (pokemon) in
            
            if let pokemon = pokemon {
                
                self?.viewModel.input.pokemon.onNext(pokemon)
                self?.pokemon = pokemon
                
                DispatchQueue.main.async {
                    
                    self?.catchButton.isHidden = true
                    
                    
                }
                
            } else {
                
                self?.requestData()
                
            }
            
        }, onError: { (error) in
            
            DispatchQueue.main.async {
                
                self.catchButton.isHidden = true
                
            }
                
        }).disposed(by: disposeBag)
        
    }
    
    private func requestData() {
        viewModel.searchPokemon(by: pokemonId).subscribe(onNext: { [weak self] (pokemon) in
        
            if let selfDefined = self {
                
                selfDefined.viewModel.input.pokemon.onNext(pokemon)
                self?.pokemon = pokemon
                
            }
        
        }, onError: { (error) in
            
            DispatchQueue.main.async {
                
                self.catchButton.isHidden = true
                
                if error._code == -1009 {
                    
                    let alert = UIAlertController(title: "No Internet connection", message: "Internet connection is required to use this app", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    
                    let alert = UIAlertController(title: "Something went wrong", message: "Please, try again", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
        }).disposed(by: disposeBag)
    }
    
    private func bindOutputs() {
        
        viewModel.output.pokemonName.drive(pokemonName.rx.text).disposed(by: disposeBag)
        viewModel.output.pokemonWeight.drive(pokemonWeight.rx.text).disposed(by: disposeBag)
        viewModel.output.pokemonHeight.drive(pokemonHeight.rx.text).disposed(by: disposeBag)
        
        viewModel.output.imageUrl.drive(onNext: { [weak self] (url) in
            guard let url = url else {
                return
            }
            
            self?.pokemonImage.af_setImage(withURL: url)
            
        }).disposed(by: disposeBag)
        
    }

}
