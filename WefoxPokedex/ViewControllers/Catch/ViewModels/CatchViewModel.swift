//
//  CatchViewModel.swift
//  WefoxPokedex
//
//  Created by Alberto García on 23/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct CatchViewModel: ViewModelType {
    
    private let pokemonService: PokemonService
    
    var input: Input
    var output: Output
    
    struct Input {
        let pokemon: AnyObserver<Pokemon>
    }
    
    struct Output {
        
        let imageUrl: Driver<URL?>
        let pokemonName: Driver<String>
        let pokemonWeight: Driver<String>
        let pokemonHeight: Driver<String>
        
    }
    
    private let pokemonResultSubject = PublishSubject<Pokemon>()
    
    init (pokemonService: PokemonService) {
        
        self.pokemonService = pokemonService
        
        let name = pokemonResultSubject.map { $0.name }.asDriver(onErrorJustReturn: "unknown")
        let weight = pokemonResultSubject.map { String($0.weight) }.asDriver(onErrorJustReturn: "unknown")
        let height = pokemonResultSubject.map { String($0.height) }.asDriver(onErrorJustReturn: "unknown")
        let imageURL = pokemonResultSubject.map { URL(string: $0.image) }.asDriver(onErrorJustReturn: nil)
        
        input = Input(pokemon: pokemonResultSubject.asObserver())
        
        output = Output(imageUrl: imageURL, pokemonName: name, pokemonWeight: weight, pokemonHeight: height)
        
    }
    
    internal func searchPokemon(by pokemonId: Int) -> Observable<Pokemon> {
        
        return self.pokemonService.searchPokemonById(pokemonId: pokemonId).observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
        
    }
    
    
    internal func loadPokemon(by pokemonId: Int) -> Observable<Pokemon?> {
        
        return Observable.just(self.pokemonService.loadPokemonBy(id: pokemonId)).observeOn(ConcurrentDispatchQueueScheduler(qos: .background))

    }
    
    func savePokemon(pokemon: Pokemon) {
        self.pokemonService.savePokemon(pokemon: pokemon)
    }
    
}
