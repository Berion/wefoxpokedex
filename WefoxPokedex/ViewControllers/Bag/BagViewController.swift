//
//  ViewController.swift
//  WefoxPokedex
//
//  Created by Alberto García on 23/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AlamofireImage

class BagViewController: UIViewController {

    private let disposeBag = DisposeBag()
    private var pokemons: [Pokemon] = []
    private var selectedPokemon: Pokemon? = nil
    private let destinationSegue = "PokemonDetailSegue"
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private lazy var viewModel: BagViewModel = {
        return BagViewModel(service: PokemonService())
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadPokemons()
    }
    
    private func loadPokemons() {
        
        viewModel.loadPokemons().subscribe(onNext: { [weak self] (pokemons) in
            
            self?.pokemons = pokemons
            
            }, onError: { (error) in
                
                DispatchQueue.main.async {

                    let alert = UIAlertController(title: "Something went wrong", message: "Please, try again", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
        }, onCompleted: {
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == destinationSegue {

            guard let destinationVC = segue.destination as? DetailViewController  else {
                return
            }


            destinationVC.pokemon = selectedPokemon


        }
    }
    
}

extension BagViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedPokemon = pokemons[indexPath.row]
        
        performSegue(withIdentifier: destinationSegue, sender: self)
    }
    
}

extension BagViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pokemonCell", for: indexPath) as? PokemonCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.nameLabel.text = ""
        cell.pokemonImageView.image = UIImage(named: "missingno")
        
        let viewModelCell = PokemonCollectionCellViewModel()
        
        viewModelCell.output.pokemonUrl.drive(onNext: { (url) in
            guard let url = url else {
                return
            }
            
            cell.pokemonImageView.af_setImage(withURL: url, placeholderImage: UIImage(named: "missingno"))
            
        }).disposed(by: disposeBag)
        
        viewModelCell.output.pokemonName.drive(cell.nameLabel.rx.text).disposed(by: disposeBag)
        
        viewModelCell.input.pokemon.onNext(pokemons[indexPath.row])
        
        return cell
        
    }
    
    
    
}
