//
//  PokemonCollectionViewCell.swift
//  WefoxPokedex
//
//  Created by Alberto García on 25/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import UIKit

class PokemonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    var pokemonId: Int!
    
}
