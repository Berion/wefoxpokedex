//
//  PokemonCollectionCellViewModel.swift
//  WefoxPokedex
//
//  Created by Alberto García on 25/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct PokemonCollectionCellViewModel: ViewModelType {
    
    var input: Input
    let output: Output
    
    struct Input {
        let pokemon: AnyObserver<Pokemon>
    }
    
    struct Output {
        
        let pokemonName: Driver<String>
        let pokemonId: Driver<Int?>
        let pokemonUrl: Driver<URL?>
        
    }
    
    private let pokemonSubject = PublishSubject<Pokemon>()
    
    init() {
        
        let id = pokemonSubject.map { $0.id }.asDriver(onErrorJustReturn: 0)
        
        let name = pokemonSubject.map { $0.name }.asDriver(onErrorJustReturn: "")
        
        let url = pokemonSubject.map { URL(string: $0.image) }.asDriver(onErrorJustReturn: nil)
        
        output = Output(pokemonName: name, pokemonId: id, pokemonUrl: url)
        
        input = Input(pokemon: pokemonSubject.asObserver())
        
    }
    
}
