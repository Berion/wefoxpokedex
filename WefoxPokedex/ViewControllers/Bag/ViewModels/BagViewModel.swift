//
//  BagViewModel.swift
//  WefoxPokedex
//
//  Created by Alberto García on 25/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

struct BagViewModel {

    private let service: PokemonService!
    
    init(service: PokemonService) {
        
        self.service = service
    }
    
    internal func loadPokemons() -> Observable<[Pokemon]> {
        
        return Observable.just(self.service.loadPokemons())
        
    }
    
}
