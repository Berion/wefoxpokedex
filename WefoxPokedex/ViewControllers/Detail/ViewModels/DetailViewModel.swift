//
//  DetailViewModel.swift
//  WefoxPokedex
//
//  Created by Alberto García on 25/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct DetailViewModel: ViewModelType {
    
    var input: Input
    var output: Output
    
    struct Input {
        let pokemon: AnyObserver<Pokemon>
    }
    
    struct Output {
        
        let imageUrl: Driver<URL?>
        let pokemonName: Driver<String>
        let pokemonWeight: Driver<String>
        let pokemonHeight: Driver<String>
        let pokemonDate: Driver<String>
        let pokemonExperience: Driver<String>
        let pokemonTypes: Driver<String>
        
    }
    
    private let pokemonResultSubject = PublishSubject<Pokemon>()
    
    init () {
        
        let name = pokemonResultSubject.map { $0.name }.asDriver(onErrorJustReturn: "unknown")
        let weight = pokemonResultSubject.map { String($0.weight) }.asDriver(onErrorJustReturn: "unknown")
        let height = pokemonResultSubject.map { String($0.height) }.asDriver(onErrorJustReturn: "unknown")
        let imageURL = pokemonResultSubject.map { URL(string: $0.image) }.asDriver(onErrorJustReturn: nil)
        
        let date = pokemonResultSubject.map { pokemon in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm dd/MM/yyyy"
            
            return formatter.string(from: pokemon.caughtDate ?? Date())
            
            }.asDriver(onErrorJustReturn: "")
        
        let experience = pokemonResultSubject.map { String($0.baseExperience) }.asDriver(onErrorJustReturn: "unknown")
        
        let types = pokemonResultSubject.map { pokemon in
            
            var types = pokemon.types.reduce("") { $0 + ", " + $1 }
            
            types.removeFirst()
            
            return  types
            
        }.asDriver(onErrorJustReturn: "unknown")
        
        
        input = Input(pokemon: pokemonResultSubject.asObserver())
        
        output = Output(imageUrl: imageURL, pokemonName: name, pokemonWeight: weight, pokemonHeight: height, pokemonDate: date, pokemonExperience: experience, pokemonTypes: types)
        
    }
    
}
