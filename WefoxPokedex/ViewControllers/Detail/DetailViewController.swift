//
//  DetailViewController.swift
//  WefoxPokedex
//
//  Created by Alberto García on 25/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AlamofireImage

class DetailViewController: UIViewController {
    
    private let viewModel = DetailViewModel()
    private let disposeBag = DisposeBag()
    
    var pokemon: Pokemon? = nil
    
    @IBOutlet weak var pokemonImage: UIImageView!
    
    @IBOutlet weak var pokemonName: UILabel!
    @IBOutlet weak var pokemonWeight: UILabel!
    @IBOutlet weak var pokemonHeight: UILabel!
    @IBOutlet weak var pokemonExperience: UILabel!
    @IBOutlet weak var caughtDate: UILabel!
    @IBOutlet weak var pokemonTypes: UILabel!
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let pokemon = pokemon else {
            return
        }
        
        bindOutputs()
        
        viewModel.input.pokemon.onNext(pokemon)
    }
    
    private func bindOutputs() {
        
        viewModel.output.pokemonName.drive(pokemonName.rx.text).disposed(by: disposeBag)
        viewModel.output.pokemonWeight.drive(pokemonWeight.rx.text).disposed(by: disposeBag)
        viewModel.output.pokemonHeight.drive(pokemonHeight.rx.text).disposed(by: disposeBag)
    
        viewModel.output.pokemonExperience.drive(pokemonExperience.rx.text).disposed(by: disposeBag)
        viewModel.output.pokemonDate.drive(caughtDate.rx.text).disposed(by: disposeBag)
        viewModel.output.pokemonTypes.drive(pokemonTypes.rx.text).disposed(by: disposeBag)
        
        viewModel.output.imageUrl.drive(onNext: { [weak self] (url) in
            guard let url = url else {
                return
            }
            
            self?.pokemonImage.af_setImage(withURL: url)
            
        }).disposed(by: disposeBag)
        
    }
    
}
