//
//  PokemonService.swift
//  WefoxPokedex
//
//  Created by Alberto García on 23/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import CoreData

protocol PokemonGettable: NetworkProtocol {
    func searchPokemonById(pokemonId: Int) -> Observable<Pokemon>
}

struct PokemonService: PokemonGettable {
    
    func searchPokemonById(pokemonId: Int) -> Observable<Pokemon> {
        
        let url = String(format: getUrl(url: .pokemonById), pokemonId)
        
        return Observable<Pokemon>.create({ (observer) -> Disposable in
            
            let requestReference = Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
                .validate()
                .responseJSON { (response) in
                    switch response.result {
                    case .success:
                        
                        guard let data = response.data else {
                            return observer.onCompleted()
                        }
                        
                        do {
                            
                            var pokemon = try JSONDecoder().decode(Pokemon.self, from: data)
                            
                            pokemon.id = pokemonId
                            
                            observer.onNext(pokemon)
                            
                        } catch {
                            observer.onError(error)
                        }
                        
                        observer.onCompleted()
                        
                    case .failure (let encodingError):
                        
                        observer.onError(encodingError)
                        
                    }
                    
                    return observer.onCompleted()
            }
            return Disposables.create(with: {
                requestReference.cancel()
            })
            
        })
    }
    
    private func getContext() -> NSManagedObjectContext? {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            print("Error retrieving AppDelegate")
            return nil
        }
        
        return appDelegate.persistentContainer.viewContext
    }
    
    func savePokemon(pokemon: Pokemon) {
        
        guard let context = getContext() else {
            return
        }

        let entity = NSEntityDescription.entity(forEntityName: "Pokemon", in: context)
        let newPokemon = NSManagedObject(entity: entity!, insertInto: context)

        newPokemon.setValue(pokemon.id, forKey: "id")
        newPokemon.setValue(pokemon.name, forKey: "name")
        newPokemon.setValue(pokemon.weight, forKey: "weight")
        newPokemon.setValue(pokemon.height, forKey: "height")
        newPokemon.setValue(pokemon.image, forKey: "image")
        newPokemon.setValue(pokemon.order, forKey: "order")
        newPokemon.setValue(Date(), forKey: "caughtDate")
        newPokemon.setValue(pokemon.baseExperience, forKey: "experience")
        newPokemon.setValue(pokemon.types, forKey: "types")
        
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
        
    }
    
    
    func loadPokemonBy(id: Int) -> Pokemon? {
        
        guard let context = getContext() else {
            return nil
        }
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Pokemon")
        
        request.returnsObjectsAsFaults = false
        
        let predicate = NSPredicate(format: "id = %i", id)
        request.predicate = predicate
        
        do {
            
            let result = try context.fetch(request)
            
            guard let pokemonManagedObject = result.first as? NSManagedObject else {
                return nil
            }
            
            if let pokemon = pokemonParse(object: pokemonManagedObject) {
                return pokemon
            }
            
            return nil
            
        } catch {
            
            print("Error finding pokemon")
            return nil
            
        }
    }
        
    func loadPokemons() -> [Pokemon] {
        
        guard let context = getContext() else {
            return []
        }
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Pokemon")
        
        request.returnsObjectsAsFaults = false
        
        let sectionSortDescriptor = NSSortDescriptor(key: "order", ascending: true)
        
        let sortDescriptors = [sectionSortDescriptor]
        
        request.sortDescriptors = sortDescriptors
        
        do {
            
            let result = try context.fetch(request)
            
            var pokemonsInBag: [Pokemon] = []
            
            for data in result as! [NSManagedObject] {
                
                if let pokemon = pokemonParse(object: data) {
                    pokemonsInBag.append(pokemon)
                }
                
            }
            
            return pokemonsInBag
            
        } catch {
            
            print("Error loading pokemon")
            return []
            
        }
        
    }
    
    
    
    private func pokemonParse(object: NSManagedObject) -> Pokemon? {
        
        guard let id = object.value(forKey: "id") as? Int else {
            return nil
        }
        
        guard let name = object.value(forKey: "name") as? String else {
            return nil
        }
        
        guard let weight = object.value(forKey: "weight") as? Float else {
            return nil
        }
        
        guard let height = object.value(forKey: "height") as? Float else {
            return nil
        }
        
        guard let image = object.value(forKey: "image") as? String else {
            return nil
        }
        
        guard let order = object.value(forKey: "order") as? Int else {
            return nil
        }
        
        guard let exp = object.value(forKey: "experience") as? Int else {
            return nil
        }
        
        guard let types = object.value(forKey: "types") as? [String] else {
            return nil
        }
        
        return Pokemon(id: id, name: name, weight: weight, height: height, image: image, order: order, caughtDate: Date(), baseExperience: exp, types: types)
        
    }
}
