//
//  Pokemon.swift
//  WefoxPokedex
//
//  Created by Alberto García on 23/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import Foundation

struct Pokemon {
    
    var id: Int?
    var name: String
    var weight: Float
    var height: Float
    var image: String
    var order: Int
    var caughtDate: Date?
    var baseExperience: Int
    var types: [String]
    
    enum CodingKeys: String, CodingKey {
        
        case id
        case name
        case weight
        case height
        case sprites
        case order
        case types
        case baseExperience = "base_experience"
    }
    
    enum SpritesCodingKeys: String, CodingKey {
        case frontDefault = "front_default"
    }
    
    enum TypesCodingKeys: String, CodingKey {
        case type
    }
    
    enum TypeCodingKeys: String, CodingKey {
        case name
    }
    
}

extension Pokemon: Decodable {
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        name = try values.decode(String.self, forKey: .name)
        weight = try values.decode(Float.self, forKey: .weight)
        height = try values.decode(Float.self, forKey: .height)
        order = try values.decode(Int.self, forKey: .order)
        baseExperience = try values.decode(Int.self, forKey: .baseExperience)
        
        let sprites = try values.nestedContainer(keyedBy: SpritesCodingKeys.self, forKey: .sprites)
        
        image = try sprites.decode(String.self, forKey: .frontDefault)
        
        types = []
        
        if var nestedTypes = try? values.nestedUnkeyedContainer(forKey: .types) {
            
            var pokemonTypes:[String] = []
            
            while (!nestedTypes.isAtEnd) {
                
                let typeContainer = try nestedTypes.nestedContainer(keyedBy: TypesCodingKeys.self)
                let type = try typeContainer.nestedContainer(keyedBy: TypeCodingKeys.self, forKey: .type)
                
                pokemonTypes.append(try type.decode(String.self, forKey: .name))
                
            }
            
            types = pokemonTypes
        }
        
        
        
        
    }
}
