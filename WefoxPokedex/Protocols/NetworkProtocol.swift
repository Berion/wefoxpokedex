//
//  NetworkProtocol.swift
//  WefoxPokedex
//
//  Created by Alberto García on 23/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import Foundation

enum APIUrls: String {
    
    case pokemonById = "pokemon/%i/"
    
    var getUrl: String {
        return "https://pokeapi.co/api/v2/" + self.rawValue
    }
    
}

protocol NetworkProtocol {
    
    func getUrl(url: APIUrls) -> String
    
}

extension NetworkProtocol {
    
    func getUrl(url: APIUrls) -> String {
        
        return url.getUrl
    }
    
}
