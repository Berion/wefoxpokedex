//
//  ViewModelType.swift
//  WefoxPokedex
//
//  Created by Alberto García on 23/04/2019.
//  Copyright © 2019 agarcia.soft. All rights reserved.
//

import Foundation

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    var input: Input { get }
    var output: Output { get }
}
